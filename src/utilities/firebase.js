import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";

const firebaseConfig = {
    apiKey: "AIzaSyC_yEPcrHN9WlFln81mLMPcFyOZOeEgqB8",
    authDomain: "modal-login-firebase-vuejs.firebaseapp.com",
    projectId: "modal-login-firebase-vuejs",
    storageBucket: "modal-login-firebase-vuejs.appspot.com",
    messagingSenderId: "909016532902",
    appId: "1:909016532902:web:acbf3e1f0964954caff167"
};

firebase.initializeApp(firebaseConfig);
const db = firebase.database();
export const chatsRef = db.ref("chats");

export default firebase;